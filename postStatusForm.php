<?php include("header.php");
?>

<div class="statusPostingSystem-container">

    <div class="infoWrapper">
        <form class="postStatus-form"  action="postStatusProcess.php" method="post">
            <h1 class="postStatusFormHeading">
                Post New Status  
            </h1>

            <div class="postStatusFormSpacing statusCodeField">
                <label class="postStatusForm-label"> Status code(required): </label> 
                <input class="postStatusForm-input" type="text" name="statusCodeInput" />
            </div>  

            <div class="postStatusFormSpacing statusField">
                <label class="postStatusForm-label"> Status(required): </label> 
                <input class="postStatusForm-input" type="text" name="statusInput" />
            </div>

            <div class="postStatusFormSpacing sharedRadioBtns">
                <label class="postStatusForm-label"> Share: </label> 
                <input  type="radio" name="share" value="Public" /> Public
                <input  type="radio" name="share" value="Friends" /> Friends
                <input  type="radio" name="share" value="OnlyMe" /> Only Me
            </div>

            <div class="postStatusFormSpacing dateField">
                <label class="postStatusForm-label"> Date: </label> 
                <input class="postStatusForm-input" type="text" name="dateInput" value="<?php echo date("d/m/y"); ?>">    
            </div>

            <div class="postStatusFormSpacing permissionCheckBox">
                <label class="postStatusForm-label"> Permission Type: </label> 
                <input  type="checkbox" name="permission[]" value="Allow Like" /> Allow Like
                <input  type="checkbox" name="permission[]" value="Allow Comment" /> Allow Comment
                <input  type="checkbox" name="permission[]" value="Allow Share" /> Allow Share
            </div>

            <div class="postStatusFormSpacing statusCodeForm-btns postStatusFormButton:hover" >
                <button class="postStatusFormButton" name="submitForm">Post</button> 
                <input  class="postStatusFormButton" type="reset" value="Reset">
            </div>

        </form> 
        <div class="postStatusLink">
            <a href="index.php"> Return to Home Page</a>
        </div>  
    </div>    
</div>
