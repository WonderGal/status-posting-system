<?php include("header.php"); ?>

<div class="statusPostingSystem-container">
    <div class="infoWrapperAbout">
        <div class=" postStatus-form aboutLink">
            <h1 class="postStatusFormHeading">
                About Me  
            </h1>
            <p>
            <bold>Q1. What special features have you done, or attempted, in creating the site that we should
                know about?</bold><br/>
            <p> Ans1. The special features for my site will be Clean design,Effective color scheme,Interactive usability.</p>
            Q2. Which parts did you have trouble with?<br/>
            <p>Ans2. The real trouble I faced while bringing this site was during the time where one or more checkbox results had to be stored in database.
                For which I had to take help from php manual and w3 school, the result was i found i had to use implode() function. Second trouble i had in seating the database from the user input's. 
                That took around few days for me to rectify all my errors and make it successful</p>
            Q3. What would you like to do better next time?<br/>
            <p> Ans3. Next time I will pay more attention on user input security issues for website.Also, I will try to use object oriented style of php.  </p>
            Q4. What references/sources you have used to help you learn how to create your Website?<br/>
            <p>Ans4. The references/sources that helped me learn in making this website are as follows:
                -Php tutorials from PLURALSIGHTS (Course name: Php Get Started ) this course helped me step by step hoe to create a form.
                -Bootstrap to get idea for the layout of site.
                -W3Schools for functions.
                -PHP regular expression 101 helped me through validation part.
                -Php manual for any functionality.</p>
            Q5. What you have learnt along the way?  <br/>
            <p>Ans5.The things that I learnt in making of this website are :
                -The use of Html and Css to style the website.
                -The use the echo and print statements,setting a variable,using an array,use of operators and operands and use of
                -Conditional statements and nested statements, 
                -Validation using regular expressions to restrict and direct the user to place in data in particular formate.
                -Creating a database connection
                -The important feature of PHP  storing forum information in a MySQL database. This allows large quantities of information to be stored and accessed quickly as needed. 
                -Manipulation of the data (INSERT, SELECT, CREATE, DELETE)from the database using query. creating a database connection </p>
        </div>
        <div class="postStatusLink">
            <a href="index.php"> Return at Home Page</a>
        </div>
    </div>
</div>