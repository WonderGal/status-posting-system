<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Status Posting System</title>
        <link rel="stylesheet" type="text/css" href="index.css">
    </head>

    <body>    
        <header class="header">
            <div class="statusPostingSystem-heading">
                Status Posting System
            </div>

            <ul class="navigationBar">
                <li class="navigationBarList" >
                    <a class="headerLink" href="postStatusForm.php"> POST A NEW STATUS</a>
                </li>

                <li class="navigationBarList">
                    <a class="headerLink" href="searchStatusForm.php"> SEARCH STATUS</a>
                </li>

                <li class="navigationBarList">
                    <a class="headerLink" href="about.php">ABOUT </a>
                </li>
            </ul>
        </header>
    </body>
</html>

