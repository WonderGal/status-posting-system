<?php include("header.php"); ?>

<div class="statusPostingSystem-container">

    <div class="infoWrapper" >
        <form class="searchStatus-Form" action="searchStatusProcess.php" method="get">  
            <h1 class="postStatusFormHeading">
                Search Status  
            </h1>

            <label class="postStatusForm-label"> Status: </label> 
            <input class="postStatusForm-input"  type="text" name="findCurrentStatus" />
            <button class="postStatusFormButton searchStatusFormSpacing" name="submit">View Status</button>
        </form>


        <div class="postStatusLink" class="searchStatusLink">
            <a href="index.php"> Return at Home Page</a>
        </div>
    </div>
</div>


