<?php

class PostStatus {

    private $dbServer;
    private $dbUserName;
    private $dbPassword;
    private $databaseConnection;

    public function __construct($dbServer, $dbUserName, $dbPassword) {
        $this->dbServer = $dbServer;
        $this->dbUserName = $dbUserName;
        $this->dbPassword = $dbPassword;
    }

    public function submitStatus() {
        if ($this->isStatusCodeValid() && $this->isStatusValid() && $this->isDateValid()) {
            $this->openDatabaseConnection();
            $this->selectDatabase();

            $statusCodeInput = $_POST["statusCodeInput"];
            $statusInput = $_POST["statusInput"];
            $share = $_POST["share"];
            $dateInput = $_POST["dateInput"];
            $permission = implode(',', $_POST["permission"]);

            $insertValues = "INSERT INTO statusInformation"
                    . "(StatusCode,Status,Share,Date,Permission)"
                    . "VALUES"
                    . "('$statusCodeInput','$statusInput','$share','$dateInput','$permission')";

            $insertResult = mysqli_query($this->databaseConnection, $insertValues);

            if (!$insertResult) {
                echo "<p>Something is wrong with ", $insertValues, "</p>";
            } else {
                echo "<p>Success</p>";
            }

            $this->closeDatabaseConnection();
        }
        return;
    }

    private function isStatusCodeValid() {
        if ($_POST["statusCodeInput"] === "" || !preg_match("/^[S]\d\d\d\d$/", $_POST["statusCodeInput"])) {
            echo"<p> Status Code is required and code Should starts with uppercase S followed by 4 numbers. </p>";
            return;
        }
        return true;
    }

    private function isStatusValid() {
        if ($_POST["statusInput"] === "" || !preg_match("/^[a-zA-Z0-9,.!? ]*$/", $_POST["statusInput"])) {
            echo"<p> Status is required and it should be alphanumeric characters including spaces,comma,period,exclamation </p>";
            return;
        }
        return true;
    }

    private function isDateValid() {
        if (!preg_match("/\d{1,2}\/\d{1,2}\/\d{2}/", $_POST["dateInput"])) {
            echo"<p> Date  should be in formate of dd/mm/yy </p>";
            return;
        }
        return true;
    }

    private function openDatabaseConnection() {
        $this->databaseConnection = mysqli_connect($this->dbServer, $this->dbUserName, $this->dbPassword);
    }

    private function closeDatabaseConnection() {
        mysqli_close($this->databaseConnection);
    }

    private function selectDatabase() {
        mysqli_select_db($this->databaseConnection, "")//provide database name
                or $this->createDatabase();
    }

    private function createDatabase() {
        $createDatabase = "CREATE DATABASE ";
        mysqli_query($this->databaseConnection, $createDatabase);
        $createTable = "CREATE TABLE statusInformation (
                           StatusCode VARCHAR(25), 
                           Status VARCHAR(100),
                           Share VARCHAR(50),
                           Date VARCHAR(40), 
                           Permission VARCHAR (50))";
        $this->selectDatabase();
        mysqli_query($this->databaseConnection, $createTable);
    }

}

$postStatus = new PostStatus("", "", "");// provide database server, database username, database password

if (isset($_POST['submitForm'])) {
    $postStatus->submitStatus();
}
?>
<div class="postStatusLink">
    <a href="index.php"> Return to Home Page</a>
</div>  
<div class="postStatusLink">
    <a href="postStatusForm.php"> Return to Post a new Status Page</a>
</div>  






