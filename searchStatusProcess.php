<?php

class SearchStatus {

    private $dbServer;
    private $dbName;
    private $dbUserName;
    private $dbPassword;
    private $databaseConnection;

    public function __construct($dbServer, $dbName, $dbUserName, $dbPassword) {
        $this->dbServer = $dbServer;
        $this->dbName = $dbName;
        $this->dbUserName = $dbUserName;
        $this->dbPassword = $dbPassword;
    }

    public function searchStatus() {
        if ($_GET["findCurrentStatus"] === "") {
            echo 'Search cannot be empty';
            return;
        }

        $this->openDatabaseConnection();
        $this->selectDatabase();
        $search = mysqli_escape_string($this->databaseConnection, $_GET['findCurrentStatus']);
        $query = "SELECT * FROM statusInformation WHERE Status='$search'";
        $result = mysqli_query($this->databaseConnection, $query);

        if (!$result) {
            echo "<p> Something is wrong with", $query, "</p>";
        }
        $row = mysql_fetch_array($result);

        if (!$row) {
            echo "No search Found";
        } else {
            while ($row) {
                echo 'Status: ' . $row['Status'];
                echo '<br />Status Code : ' . $row['StatusCode'];
                echo '<br /> Share: ' . $row['Share'];
                echo '<br /> DatePosted: ' . $row['Date'];
                echo '<br /> Permission: ' . $row['Permission'];
            }
            mysqli_free_result($result);
        }
        $this->closeDatabaseConnection();
    }

    private function openDatabaseConnection() {
        $this->databaseConnection = mysqli_connect($this->dbServer, $this->dbUserName, $this->dbPassword, $this->dbName);
    }

    private function closeDatabaseConnection() {
        mysqli_close($this->databaseConnection);
    }

    private function selectDatabase() {
        mysqli_select_db($this->databaseConnection, $this->dbName)
                or die('Database not available');
    }

}

$searchStatus = new SearchStatus('', '', '', '');//provide database server, database username, database password,database name

if (isset($_GET['submit'])) {
    $searchStatus->searchStatus();
}
?>
<div class="postStatusLink">
    <a href="index.php"> Return to Home Page</a>
</div>  
<div class="postStatusLink">
    <a href="postStatusForm.php"> Return to Post a new Status Page</a>
</div>